package controle;

import dao.ProdutoDao;
import java.util.ArrayList;
import java.util.List;
import modelos.Produto;

public class ProdutoControle {

    private List<Produto> produtos;
    private ProdutoDao produtoDao;
    private Produto produto;

    public ProdutoControle() {
        produtos = new ArrayList<Produto>();
        produtoDao = new ProdutoDao();
        produto = new Produto();
    }

    public void entradaProduto(String nome, int quantidade) {
        ProdutoDao produtoDao = new ProdutoDao();
        Produto p = produtoDao.findByName(nome);
        produtoDao.entradaProduto(p, quantidade);
    }

    public void saidaProduto(String nome, int quantidade) {
        ProdutoDao produtoDao = new ProdutoDao();
        Produto p = produtoDao.findByName(nome);
        produtoDao.saidaProduto(p, quantidade);
    }

    public List<Produto> getProdutos() throws Exception {
        ProdutoDao produtoDao = new ProdutoDao();
        this.produtos = produtoDao.findAll();
        return this.produtos;
    }
    
    public boolean salvaProduto(){
        try {
            produtoDao.salvar(this.produto);
            return true;
        } catch (Exception e) {
            return false;            
        }
        
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

    public ProdutoDao getProdutoDao() {
        return produtoDao;
    }

    public void setProdutoDao(ProdutoDao produtoDao) {
        this.produtoDao = produtoDao;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }
}
