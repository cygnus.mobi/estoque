
package controle;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import java.io.FileOutputStream;
import java.io.OutputStream;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelos.Produto;

public class Relatorios {
    
    private String cabecalho;
    private Date data;
    
    public Relatorios(){
        data = new Date();
        SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
        String data = formatador.format(this.data);
        cabecalho = "ESTADO DO RIO GRANDE DO NORTE \nSERVIÇO AUTÔNOMO DE ÁGUA E ESGOSTO \nAV. RIO BRANCO, 609 \nSANTA CRUZ - "+data+"\n\n\n\n\n";
    }
        
    public Document montaCabecalho(Document doc) throws DocumentException, BadElementException, IOException{        
        PdfPTable tabela = new PdfPTable(new float[]{0.2f, 0.8f}); //cria uma tabela com 2 colunas 1a 20% e 2a 80% do espaço
        //tabela.setHorizontalAlignment(Element.ALIGN_CENTER);
        Image i = Image.getInstance(getClass().getResource("/src/saae.png"));
        i.scaleAbsolute(64,64);
        PdfPCell celula1 = new PdfPCell(i); //cria uma celula com parametro de Image.getInstance com o caminho da imagem do cabeçalho
        //FONTE
        Font f = new Font(FontFamily.TIMES_ROMAN, 12, Font.BOLD);
        Paragraph p = new Paragraph(cabecalho, f);                        
        PdfPCell celula2 = new PdfPCell(p); //adiciona o paragrafo com o titulo na segunda celula.
        celula1.setBorder(-1); // aqui vc tira as bordas da celula 
        celula2.setBorder(-1);
        tabela.addCell(celula1); //aqui adiciona as celulas na tabela.
        tabela.addCell(celula2);
        doc.add(tabela); // coloca a tabela na pagina do PDF.        
        return doc;
    }
    
    public void gerarPdf(List<Produto> produtos, int numeroColunas) throws FileNotFoundException, DocumentException, IOException{
        Document doc = null;
        OutputStream os = null;         
        JFrame janelaSalvar = new JFrame();
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Ecolha um local para salvar o relatório"); 
        String caminho = "";

        int userSelection = fileChooser.showSaveDialog(janelaSalvar);

        if (userSelection == JFileChooser.APPROVE_OPTION) {
            File fileToSave = fileChooser.getSelectedFile();            
            try {               
                //cria o documento tamanho A4, margens de 2,54cm
                doc = new Document(PageSize.A4, 72, 72, 72, 72);  
                //cria a stream de saída (SETA CAMINHO ESCOLHIDO)
                caminho = fileChooser.getCurrentDirectory()+"\\"+fileChooser.getName(fileToSave) + ".pdf";
                os = new FileOutputStream(caminho);                  
                //associa a stream de saída ao 
                PdfWriter.getInstance(doc, os);  
                //abre o documento
                doc.open();                
                //PREPARA CABEÇALHO DO RELATÓRIO
                doc = montaCabecalho(doc);                                                                
                //INSERIR TABELA COM A LISTA PASSADA POR PARAMETRO
                PdfPTable tabela = new PdfPTable(new float[]{0.8f, 0.1f, 0.1f});                
                tabela.setTotalWidth(500); //LARGURA DA TABELA
                tabela.setLockedWidth(true);
                Font f = new Font(FontFamily.TIMES_ROMAN, 12, Font.BOLD);
                Paragraph h = new Paragraph("LISTA DE TODOS OS PRODUTOS EM ESTOQUE", f);                                
                PdfPCell header = new PdfPCell(h);
                header.setColspan(3);
                header.setHorizontalAlignment(Element.ALIGN_CENTER);
                header.setVerticalAlignment(Element.ALIGN_MIDDLE);
                header.setBackgroundColor(new BaseColor(235, 235, 235));
                tabela.addCell(header);                                                       
                //PRIMEIRA LINHA NOME, UND E QUANT
                //NOME
                Paragraph p1 = new Paragraph("NOME", f);
                PdfPCell c = new PdfPCell(p1);    
                c.setHorizontalAlignment(Element.ALIGN_CENTER);
                c.setVerticalAlignment(Element.ALIGN_MIDDLE);
                tabela.addCell(c);
                //UNID
                p1 = new Paragraph("UNID", f);
                c = new PdfPCell(p1);                    
                c.setHorizontalAlignment(Element.ALIGN_CENTER);
                c.setVerticalAlignment(Element.ALIGN_MIDDLE);
                tabela.addCell(c);
                //QUANTIDADE
                p1 = new Paragraph("QUANT", f);
                c = new PdfPCell(p1);   
                c.setHorizontalAlignment(Element.ALIGN_CENTER);
                c.setVerticalAlignment(Element.ALIGN_MIDDLE);
                tabela.addCell(c);  
                //FONTE DOS ELEMENTOS DA TABELA
                f = new Font(FontFamily.HELVETICA, 10, Font.NORMAL);
                int linhasZebradas = 0;                
                BaseColor cor;                    
                for(Produto p:produtos){
                    cor = new BaseColor(255, 255, 255);
                    if(linhasZebradas % 2 == 0){                        
                        cor = new BaseColor(240, 240, 240);
                    }                    
                    //NOME
                    Paragraph paragrafo = new Paragraph(p.getNome(), f);
                    PdfPCell celulaNome = new PdfPCell(paragrafo);    
                    celulaNome.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    celulaNome.setBackgroundColor(cor);
                    tabela.addCell(celulaNome);
                    //UNID
                    paragrafo = new Paragraph(p.getUnidade(), f);
                    PdfPCell celulaUnid = new PdfPCell(paragrafo);                    
                    celulaUnid.setHorizontalAlignment(Element.ALIGN_CENTER);
                    celulaUnid.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    celulaUnid.setBackgroundColor(cor);
                    tabela.addCell(celulaUnid);
                    //QUANTIDADE
                    paragrafo = new Paragraph(String.valueOf(p.getQuantidade()), f);
                    PdfPCell celulaQuantidade = new PdfPCell(paragrafo);   
                    celulaQuantidade.setHorizontalAlignment(Element.ALIGN_CENTER);
                    celulaQuantidade.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    celulaQuantidade.setBackgroundColor(cor);
                    tabela.addCell(celulaQuantidade);                                                            
                    
                    linhasZebradas ++;
                }
                doc.add(tabela);
            } finally {
                if (doc != null) {
                    //fechamento do documento
                    doc.close();
                }
                if (os != null) {
                   //fechamento da stream de saída
                   os.close();
                }
                JOptionPane.showMessageDialog(null, "Relatório salvo em:\n"+caminho,
                    "Atenção", JOptionPane.INFORMATION_MESSAGE);
            }                                      
        }
    }                
}
