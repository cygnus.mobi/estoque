package dao;

import modelos.Produto;

public class ProdutoDao extends GenericDao<Produto> {

    public ProdutoDao() {
        super();
    }

    public void salvar(Produto produto) {
        save(produto);
    }

    public void alterar(Produto produto) {
        update(produto);
    }

    public void excluir(long id) {
        Produto p = findById(id);
        delete(p);
    }

    public void entradaProduto(Produto produto, int quantidade) {
        Produto p = findById(produto.getId());
        p.setQuantidade(produto.getQuantidade() + quantidade);
        alterar(p);
    }

    public void saidaProduto(Produto produto, int quantidade) {
        Produto p = findById(produto.getId());
        p.setQuantidade(produto.getQuantidade() - quantidade);
        alterar(p);
    }
}
