package estoque;


import java.awt.BorderLayout;
import java.awt.Color;
import static java.awt.Color.GREEN;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import static java.awt.Frame.MAXIMIZED_BOTH;
import java.awt.Toolkit;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import visao.Principal;
import visao.janelaLogin;

public class Estoque {

    public static void main(String[] args) throws IOException {        
        try {
            UIManager.setLookAndFeel( "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
            
            //UIManager.put("nimbusBase", new Color(30,144,255));
            //U0IManager.put("nimbusBlueGrey", new Color(30,144,255));
            UIManager.put("control", new Color(245,245,245));
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Estoque.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) { 
            Logger.getLogger(Estoque.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(Estoque.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(Estoque.class.getName()).log(Level.SEVERE, null, ex);
        }        
        //INICIA O SISTEMA COM LOGIN
        new janelaLogin(new javax.swing.JFrame(), true).setVisible(true);
    }
}
