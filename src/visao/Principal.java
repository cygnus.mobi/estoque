package visao;

import com.itextpdf.text.DocumentException;
import controle.ProdutoControle;
import controle.Relatorios;
import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import modelos.Produto;
import org.jboss.jandex.Main;

public class Principal extends javax.swing.JFrame {
    
    private ProdutoControle prodControle;
    private String primeiraOpComboBox;
    private DefaultTableModel modeloTabela;

    public Principal() throws IOException {
        initComponents();
        //setLocationRelativeTo(this);

        primeiraOpComboBox = "Seleciona um produto...";
        prodControle = new ProdutoControle();
        modeloTabela = constroiModelo();
        popularJtable();

        ImageIcon imagem = new ImageIcon(Main.class.getResource("/src/saae.png"));
        Image imag = imagem.getImage().getScaledInstance(logoJanelaPrincipal.getWidth(), logoJanelaPrincipal.getHeight(), Image.SCALE_DEFAULT);
        logoJanelaPrincipal.setIcon(new ImageIcon(imag));
        
        /*ImageIcon imagem2 = new ImageIcon(Main.class.getResource("/src/iconeMais.png"));
        Image image = imagem2.getImage().getScaledInstance(jLabelIconeMais.getWidth(), jLabelIconeMais.getHeight(), Image.SCALE_DEFAULT);
        jLabelIconeMais.setIcon(new ImageIcon(image));*/

        URL url = this.getClass().getResource("/src/saae_64x64.png");
        Image iconeTitulo = Toolkit.getDefaultToolkit().getImage(url);

        Image i = ImageIO.read(getClass().getResource("/src/saae_64x64.png"));
        this.setIconImage(i);
        
        botaoEntradaProduto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/src/entrada.png")));
        botaoSaidaProduto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/src/saida.png")));
        botaoSalvarNovoProd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/src/save.png")));
        jButtonSalvarEntrada.setIcon(new javax.swing.ImageIcon(getClass().getResource("/src/save.png")));
        jButtonSalvarSaida.setIcon(new javax.swing.ImageIcon(getClass().getResource("/src/save.png")));
        jButtonCancelarEntrada.setIcon(new javax.swing.ImageIcon(getClass().getResource("/src/cancel.png")));
        jButtonCancelarSaida.setIcon(new javax.swing.ImageIcon(getClass().getResource("/src/cancel.png")));
        botaoCancelar_janelaNovoProd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/src/cancel.png")));
    }

    public void populaComboBoxEntrada() {
        jComboBoxProdutos.addItem(primeiraOpComboBox);
        List<Produto> produtos;
        try {
            produtos = prodControle.getProdutos();
            for (Produto p : produtos) {
                jComboBoxProdutos.addItem(p.getNome());
            }
        } catch (Exception ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void populaComboBoxSaida() {
        jComboBoxProdutos1.addItem(primeiraOpComboBox);
        List<Produto> produtos;
        try {
            produtos = prodControle.getProdutos();
            for (Produto p : produtos) {
                jComboBoxProdutos1.addItem(p.getNome());
            }
        } catch (Exception ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public DefaultTableModel constroiModelo() {
        DefaultTableModel modelo = (DefaultTableModel) jTableProdutos.getModel();
        modelo.addColumn("ID");
        modelo.addColumn("PRODUTO");
        //modelo.addColumn("Unidade");
        modelo.addColumn("QUANTIDADE");        
        return modelo;
    }

    public void popularJtable() {
        
        //Remove todas as linhas para preencher a tabela novamente
        while (modeloTabela.getRowCount()>0){
            modeloTabela.removeRow(0);
        }                

        try {
            List<Produto> produtos = prodControle.getProdutos();
            for (Produto p : produtos) {               
                if (p.getId() < 10) {
                    this.modeloTabela.addRow(new Object[]{"00" + p.getId(), p.getNome(), /*p.getUnidade(),*/ p.getQuantidade()});
                } else if (p.getId() < 100) {
                    this.modeloTabela.addRow(new Object[]{"0" + p.getId(), p.getNome(), /*p.getUnidade(),*/ p.getQuantidade()});
                } else {
                    this.modeloTabela.addRow(new Object[]{p.getId(), p.getNome(), /*p.getUnidade(),*/ p.getQuantidade()});
                }
            }            
            //TAMANHO DAS COLUNAS DA TABELA
            this.jTableProdutos.getColumnModel().getColumn(0).setPreferredWidth(90);
            this.jTableProdutos.getColumnModel().getColumn(1).setPreferredWidth(753);
            this.jTableProdutos.getColumnModel().getColumn(2).setPreferredWidth(160);         
            
            //LINHAS ZEBRADAS
            this.jTableProdutos.setDefaultRenderer(Object.class, 
                    new DefaultTableCellRenderer() { 
                        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) { 
                            super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column); 
                            if (row % 2 == 1) { 
                                setBackground(new java.awt.Color(204, 229, 255)); 
                            } else { 
                                setBackground(null); } return this; } 
                    });                                   

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Erro popular tabela --- " + ex);
        }
    } 
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        entradaProduto = new javax.swing.JDialog();
        jPanel2 = new javax.swing.JPanel();
        jLabelProduto = new javax.swing.JLabel();
        jComboBoxProdutos = new javax.swing.JComboBox<>();
        jLabelQuantidade = new javax.swing.JLabel();
        jTextFieldQuantidade = new javax.swing.JFormattedTextField();
        jButtonSalvarEntrada = new javax.swing.JButton();
        jButtonCancelarEntrada = new javax.swing.JButton();
        jLabelIconeMais = new javax.swing.JLabel();
        saidaProduto = new javax.swing.JDialog();
        jPanel3 = new javax.swing.JPanel();
        jLabelProduto1 = new javax.swing.JLabel();
        jComboBoxProdutos1 = new javax.swing.JComboBox<>();
        jLabelQuantidade1 = new javax.swing.JLabel();
        jTextFieldQuantidade1 = new javax.swing.JFormattedTextField();
        jButtonSalvarSaida = new javax.swing.JButton();
        jButtonCancelarSaida = new javax.swing.JButton();
        jLabelIconeMenos = new javax.swing.JLabel();
        cadastrarNovoProduto = new javax.swing.JDialog();
        jPanel4 = new javax.swing.JPanel();
        jLabelProduto2 = new javax.swing.JLabel();
        jLabelQuantidade2 = new javax.swing.JLabel();
        jTextFieldQuantidadeProd = new javax.swing.JFormattedTextField();
        botaoSalvarNovoProd = new javax.swing.JButton();
        jLabelQuantidade3 = new javax.swing.JLabel();
        jTextFieldUnid = new javax.swing.JFormattedTextField();
        jTextFieldNome = new javax.swing.JTextField();
        botaoCancelar_janelaNovoProd = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        logoJanelaPrincipal = new javax.swing.JLabel();
        botaoEntradaProduto = new javax.swing.JButton();
        botaoSaidaProduto = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableProdutos = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        menu = new javax.swing.JMenuBar();
        opNovo = new javax.swing.JMenu();
        opNovoProduto = new javax.swing.JMenuItem();
        relatorio = new javax.swing.JMenu();
        listarTodosPdf = new javax.swing.JMenuItem();
        sobre = new javax.swing.JMenu();

        entradaProduto.setBackground(new java.awt.Color(245, 245, 245));
        entradaProduto.setResizable(false);

        jPanel2.setBackground(new java.awt.Color(245, 245, 245));
        jPanel2.setPreferredSize(new java.awt.Dimension(728, 278));

        jLabelProduto.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabelProduto.setText("Produto:");

        jComboBoxProdutos.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jComboBoxProdutos.setMaximumRowCount(12);
        jComboBoxProdutos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxProdutosActionPerformed(evt);
            }
        });

        jLabelQuantidade.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabelQuantidade.setText("Quantidade:");

        jTextFieldQuantidade.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("####"))));
        jTextFieldQuantidade.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jButtonSalvarEntrada.setBackground(new java.awt.Color(2, 176, 89));
        jButtonSalvarEntrada.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButtonSalvarEntrada.setForeground(new java.awt.Color(255, 255, 255));
        jButtonSalvarEntrada.setText("Salvar");
        jButtonSalvarEntrada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarEntradaActionPerformed(evt);
            }
        });

        jButtonCancelarEntrada.setBackground(new java.awt.Color(220, 0, 0));
        jButtonCancelarEntrada.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButtonCancelarEntrada.setForeground(new java.awt.Color(255, 255, 255));
        jButtonCancelarEntrada.setText("Cancelar");
        jButtonCancelarEntrada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarEntradaActionPerformed(evt);
            }
        });

        jLabelIconeMais.setIcon(new javax.swing.ImageIcon(getClass().getResource("/src/iconeMais.png"))); // NOI18N
        jLabelIconeMais.setText("jLabel1");
        jLabelIconeMais.setOpaque(true);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelProduto)
                    .addComponent(jLabelQuantidade))
                .addGap(33, 33, 33)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addComponent(jButtonSalvarEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(52, 52, 52)
                            .addComponent(jButtonCancelarEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(44, 44, 44)
                            .addComponent(jLabelIconeMais, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jComboBoxProdutos, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(48, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelProduto)
                    .addComponent(jComboBoxProdutos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelQuantidade)
                    .addComponent(jTextFieldQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonSalvarEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonCancelarEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelIconeMais, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(65, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout entradaProdutoLayout = new javax.swing.GroupLayout(entradaProduto.getContentPane());
        entradaProduto.getContentPane().setLayout(entradaProdutoLayout);
        entradaProdutoLayout.setHorizontalGroup(
            entradaProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(entradaProdutoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 700, Short.MAX_VALUE)
                .addContainerGap())
        );
        entradaProdutoLayout.setVerticalGroup(
            entradaProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, entradaProdutoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, 241, 241, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        saidaProduto.setBackground(new java.awt.Color(245, 245, 245));

        jPanel3.setBackground(new java.awt.Color(245, 245, 245));

        jLabelProduto1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabelProduto1.setText("Produto:");

        jComboBoxProdutos1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jComboBoxProdutos1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxProdutos1ActionPerformed(evt);
            }
        });

        jLabelQuantidade1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabelQuantidade1.setText("Quantidade:");

        jTextFieldQuantidade1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("####"))));
        jTextFieldQuantidade1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jButtonSalvarSaida.setBackground(new java.awt.Color(2, 176, 89));
        jButtonSalvarSaida.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButtonSalvarSaida.setForeground(new java.awt.Color(255, 255, 255));
        jButtonSalvarSaida.setText("Salvar");
        jButtonSalvarSaida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarSaidaActionPerformed(evt);
            }
        });

        jButtonCancelarSaida.setBackground(new java.awt.Color(220, 0, 0));
        jButtonCancelarSaida.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButtonCancelarSaida.setForeground(new java.awt.Color(255, 255, 255));
        jButtonCancelarSaida.setText("Cancelar");
        jButtonCancelarSaida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarSaidaActionPerformed(evt);
            }
        });

        jLabelIconeMenos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/src/iconeMenos.png"))); // NOI18N
        jLabelIconeMenos.setText("jLabel1");
        jLabelIconeMenos.setOpaque(true);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelProduto1)
                    .addComponent(jLabelQuantidade1))
                .addGap(33, 33, 33)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldQuantidade1, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addComponent(jButtonSalvarSaida, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(52, 52, 52)
                            .addComponent(jButtonCancelarSaida, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelIconeMenos, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jComboBoxProdutos1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelProduto1)
                    .addComponent(jComboBoxProdutos1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelQuantidade1)
                    .addComponent(jTextFieldQuantidade1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonSalvarSaida, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonCancelarSaida, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabelIconeMenos, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(94, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout saidaProdutoLayout = new javax.swing.GroupLayout(saidaProduto.getContentPane());
        saidaProduto.getContentPane().setLayout(saidaProdutoLayout);
        saidaProdutoLayout.setHorizontalGroup(
            saidaProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(saidaProdutoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        saidaProdutoLayout.setVerticalGroup(
            saidaProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(saidaProdutoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        cadastrarNovoProduto.setBackground(new java.awt.Color(245, 245, 245));

        jPanel4.setBackground(new java.awt.Color(245, 245, 245));

        jLabelProduto2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabelProduto2.setText("Nome:");

        jLabelQuantidade2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabelQuantidade2.setText("Quantidade em estoque:");

        jTextFieldQuantidadeProd.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("####"))));
        jTextFieldQuantidadeProd.setText("0");
        jTextFieldQuantidadeProd.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        botaoSalvarNovoProd.setBackground(new java.awt.Color(2, 176, 89));
        botaoSalvarNovoProd.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        botaoSalvarNovoProd.setForeground(new java.awt.Color(255, 255, 255));
        botaoSalvarNovoProd.setText("Salvar");
        botaoSalvarNovoProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoSalvarNovoProdActionPerformed(evt);
            }
        });

        jLabelQuantidade3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabelQuantidade3.setText("Unidade");

        jTextFieldUnid.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("####"))));
        jTextFieldUnid.setText("Und");
        jTextFieldUnid.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jTextFieldNome.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        botaoCancelar_janelaNovoProd.setBackground(new java.awt.Color(220, 0, 0));
        botaoCancelar_janelaNovoProd.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        botaoCancelar_janelaNovoProd.setForeground(new java.awt.Color(255, 255, 255));
        botaoCancelar_janelaNovoProd.setText("Cancelar");
        botaoCancelar_janelaNovoProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoCancelar_janelaNovoProdActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabelProduto2)
                        .addGap(18, 18, 18)
                        .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, 539, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabelQuantidade3)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldUnid, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(botaoSalvarNovoProd, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(55, 55, 55)
                                .addComponent(botaoCancelar_janelaNovoProd, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabelQuantidade2)
                        .addGap(18, 18, 18)
                        .addComponent(jTextFieldQuantidadeProd, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(81, 81, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelProduto2)
                    .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelQuantidade2)
                    .addComponent(jTextFieldQuantidadeProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelQuantidade3)
                    .addComponent(jTextFieldUnid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botaoSalvarNovoProd, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botaoCancelar_janelaNovoProd, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout cadastrarNovoProdutoLayout = new javax.swing.GroupLayout(cadastrarNovoProduto.getContentPane());
        cadastrarNovoProduto.getContentPane().setLayout(cadastrarNovoProdutoLayout);
        cadastrarNovoProdutoLayout.setHorizontalGroup(
            cadastrarNovoProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cadastrarNovoProdutoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        cadastrarNovoProdutoLayout.setVerticalGroup(
            cadastrarNovoProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cadastrarNovoProdutoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Controle de estoque");
        setAutoRequestFocus(false);
        setBackground(new java.awt.Color(245, 245, 245));
        setName("jf_principal"); // NOI18N
        setSize(new java.awt.Dimension(0, 0));

        logoJanelaPrincipal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        logoJanelaPrincipal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/src/saae.png"))); // NOI18N
        logoJanelaPrincipal.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        logoJanelaPrincipal.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        logoJanelaPrincipal.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        botaoEntradaProduto.setBackground(new java.awt.Color(2, 176, 89));
        botaoEntradaProduto.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        botaoEntradaProduto.setForeground(new java.awt.Color(255, 255, 255));
        botaoEntradaProduto.setText("Entrada de produto");
        botaoEntradaProduto.setToolTipText("Entra com quantidade de produto");
        botaoEntradaProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoEntradaProdutoActionPerformed(evt);
            }
        });

        botaoSaidaProduto.setBackground(new java.awt.Color(2, 176, 89));
        botaoSaidaProduto.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        botaoSaidaProduto.setForeground(new java.awt.Color(255, 255, 255));
        botaoSaidaProduto.setText("Saída de produto");
        botaoSaidaProduto.setToolTipText("Sai com quantidade de produto");
        botaoSaidaProduto.setMaximumSize(new java.awt.Dimension(207, 31));
        botaoSaidaProduto.setMinimumSize(new java.awt.Dimension(207, 31));
        botaoSaidaProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoSaidaProdutoActionPerformed(evt);
            }
        });

        jTableProdutos.setFont(new java.awt.Font("arial", 0, 16));
        jTableProdutos.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jTableProdutos.setEnabled(false);
        jTableProdutos.setSelectionBackground(new java.awt.Color(204, 204, 204));
        jTableProdutos.getTableHeader().setResizingAllowed(false);
        jTableProdutos.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(jTableProdutos);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N
        jLabel2.setText("<html>Copyright <a>CYGNUS</a></html>");

        jLabel3.setFont(new java.awt.Font("Arial", 1, 36)); // NOI18N
        jLabel3.setText("Controle de estoque");

        jLabel4.setFont(new java.awt.Font("Arial", 0, 24)); // NOI18N
        jLabel4.setText("Serviço Autônomo de Água e Esgoto");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(170, 925, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(288, 288, 288)
                .addComponent(logoJanelaPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jLabel3)))
                .addGap(0, 233, Short.MAX_VALUE))
            .addComponent(jScrollPane1)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(166, 166, 166)
                .addComponent(botaoEntradaProduto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(botaoSaidaProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(171, 171, 171))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(logoJanelaPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 44, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botaoEntradaProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botaoSaidaProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        menu.setBackground(new java.awt.Color(30, 144, 255));
        menu.setToolTipText("");
        menu.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        menu.setPreferredSize(new java.awt.Dimension(140, 30));

        opNovo.setText("Novo");
        opNovo.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N

        opNovoProduto.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        opNovoProduto.setText("Produto no estoque");
        opNovoProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opNovoProdutoActionPerformed(evt);
            }
        });
        opNovo.add(opNovoProduto);

        menu.add(opNovo);

        relatorio.setText("Relatório");
        relatorio.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N

        listarTodosPdf.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        listarTodosPdf.setText("Listagem de todos os produtos");
        listarTodosPdf.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listarTodosPdfActionPerformed(evt);
            }
        });
        relatorio.add(listarTodosPdf);

        menu.add(relatorio);

        sobre.setText("Sobre");
        sobre.setEnabled(false);
        sobre.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        menu.add(sobre);

        setJMenuBar(menu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1024, Short.MAX_VALUE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 688, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBoxProdutosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxProdutosActionPerformed
    }//GEN-LAST:event_jComboBoxProdutosActionPerformed

    private void jButtonSalvarEntradaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarEntradaActionPerformed
        String quant = jTextFieldQuantidade.getText();
        if (jComboBoxProdutos.getSelectedItem().equals(primeiraOpComboBox)) {
            JOptionPane.showMessageDialog(null, "É necessário selecionar um produto!",
                    "Atenção", JOptionPane.INFORMATION_MESSAGE);
            jComboBoxProdutos.setBorder(BorderFactory.createLineBorder(Color.RED));
        } else if (quant.isEmpty()) {
            JOptionPane.showMessageDialog(null, "É necessário inserir uma quantidade!",
                    "Atenção", JOptionPane.INFORMATION_MESSAGE);

            jTextFieldQuantidade.setBorder(BorderFactory.createLineBorder(Color.RED));
        } else {
            int quantidade = Math.abs(Integer.parseInt(quant));
            String nome = jComboBoxProdutos.getSelectedItem().toString();
            this.prodControle.entradaProduto(nome, quantidade);

            int valor = JOptionPane.showConfirmDialog(null, "Entrada realizada com sucesso! \nDeseja dar entrada em outro produto?", "Atenção", JOptionPane.YES_NO_OPTION);
            if (valor == JOptionPane.YES_OPTION) {
                fecharResetarDialogEntradaProdutos();
                entradaProduto.setVisible(true);
            } else {
                fecharResetarDialogEntradaProdutos();
                popularJtable();
            }
        }
    }//GEN-LAST:event_jButtonSalvarEntradaActionPerformed

    public void fecharResetarDialogEntradaProdutos() {
        entradaProduto.dispose();
        jTextFieldQuantidade.setText("");
        jTextFieldQuantidade.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        jComboBoxProdutos.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        jComboBoxProdutos.setSelectedIndex(0);
    }
    
    public void fecharResetarDialogNovoProduto(){
        entradaProduto.dispose();
        jTextFieldNome.setText("");
        jTextFieldQuantidadeProd.setText("0");
        jTextFieldUnid.setText("Und");
        jTextFieldQuantidadeProd.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        
    }

    private void jComboBoxProdutos1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxProdutos1ActionPerformed
    }//GEN-LAST:event_jComboBoxProdutos1ActionPerformed

    private void jButtonSalvarSaidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarSaidaActionPerformed
        String quant = jTextFieldQuantidade1.getText();
        if (jComboBoxProdutos1.getSelectedItem().equals(primeiraOpComboBox)) {
            JOptionPane.showMessageDialog(null, "É necessário selecionar um produto!",
                    "Atenção", JOptionPane.INFORMATION_MESSAGE);
            jComboBoxProdutos1.setBorder(BorderFactory.createLineBorder(Color.RED));
        } else if (quant.isEmpty()) {
            JOptionPane.showMessageDialog(null, "É necessário inserir uma quantidade!",
                    "Atenção", JOptionPane.INFORMATION_MESSAGE);

            jTextFieldQuantidade1.setBorder(BorderFactory.createLineBorder(Color.RED));
        } else {
            int quantidade = Math.abs(Integer.parseInt(quant));
            String nome = jComboBoxProdutos1.getSelectedItem().toString();
            this.prodControle.saidaProduto(nome, quantidade);

            int valor = JOptionPane.showConfirmDialog(null, "Saida realizada com sucesso! \nDeseja dar saida em outro produto?", "Atenção", JOptionPane.YES_NO_OPTION);
            if (valor == JOptionPane.YES_OPTION) {
                fecharResetarDialogSaidaProdutos();
                saidaProduto.setVisible(true);
            } else {
                fecharResetarDialogSaidaProdutos();
                popularJtable();
            }
        }
    }//GEN-LAST:event_jButtonSalvarSaidaActionPerformed

    private void botaoSaidaProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoSaidaProdutoActionPerformed
        populaComboBoxSaida();
        saidaProduto.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        saidaProduto.setSize(720, 278);
        saidaProduto.setResizable(false);
        saidaProduto.setLocationRelativeTo(null);
        saidaProduto.setTitle("Saída de produtos");
        saidaProduto.setModal(true);
        saidaProduto.setVisible(true);
    }//GEN-LAST:event_botaoSaidaProdutoActionPerformed

    private void botaoEntradaProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoEntradaProdutoActionPerformed
        populaComboBoxEntrada();
        entradaProduto.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        entradaProduto.setSize(720, 278);
        entradaProduto.setResizable(false);
        entradaProduto.setLocationRelativeTo(null);
        entradaProduto.setTitle("Entrada de produtos");
        entradaProduto.setModal(true);
        entradaProduto.setVisible(true);
    }//GEN-LAST:event_botaoEntradaProdutoActionPerformed

    private void opNovoProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opNovoProdutoActionPerformed
        cadastrarNovoProduto.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        cadastrarNovoProduto.setSize(720, 300);
        cadastrarNovoProduto.setResizable(false);
        cadastrarNovoProduto.setLocationRelativeTo(null);
        cadastrarNovoProduto.setTitle("Cadastrar novo produto no estoque");
        cadastrarNovoProduto.setModal(true);
        cadastrarNovoProduto.setVisible(true);
    }//GEN-LAST:event_opNovoProdutoActionPerformed

    private void botaoSalvarNovoProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoSalvarNovoProdActionPerformed
        prodControle.setProduto(new Produto());              
        prodControle.getProduto().setNome(jTextFieldNome.getText().toUpperCase());        
        prodControle.getProduto().setQuantidade(Integer.parseInt(jTextFieldQuantidadeProd.getText()));
        prodControle.getProduto().setUnidade(jTextFieldUnid.getText());
        boolean salvou = prodControle.salvaProduto();
        if(salvou){            
            int valor = JOptionPane.showConfirmDialog(null, prodControle.getProduto().getNome()+" cadastrado com sucesso! \nDeseja cadastrar outro produto?", "Atenção", JOptionPane.YES_NO_OPTION);
            if (valor == JOptionPane.YES_OPTION) {
                fecharResetarDialogNovoProduto();
                cadastrarNovoProduto.setVisible(true);
            } else {
                fecharResetarDialogNovoProduto();
                popularJtable();
            }                        
        }else{
            JOptionPane.showMessageDialog(null, "ERRO AO SALVAR O PRODUTO",
                    "Atenção", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_botaoSalvarNovoProdActionPerformed

    private void listarTodosPdfActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listarTodosPdfActionPerformed
        Relatorios listarTodosPdf = new Relatorios();
        try {            
            List<Produto> produtos = prodControle.getProdutos();
            listarTodosPdf.gerarPdf(produtos, 3);
        } catch (DocumentException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_listarTodosPdfActionPerformed

    private void botaoCancelar_janelaNovoProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoCancelar_janelaNovoProdActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_botaoCancelar_janelaNovoProdActionPerformed

    private void jButtonCancelarSaidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarSaidaActionPerformed
        fecharResetarDialogSaidaProdutos();
    }//GEN-LAST:event_jButtonCancelarSaidaActionPerformed

    private void jButtonCancelarEntradaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarEntradaActionPerformed
        fecharResetarDialogEntradaProdutos();
    }//GEN-LAST:event_jButtonCancelarEntradaActionPerformed

    public void fecharResetarDialogSaidaProdutos() {
        saidaProduto.dispose();
        jTextFieldQuantidade1.setText("");
        jTextFieldQuantidade1.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        jComboBoxProdutos1.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        jComboBoxProdutos1.setSelectedIndex(0);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botaoCancelar_janelaNovoProd;
    private javax.swing.JButton botaoEntradaProduto;
    private javax.swing.JButton botaoSaidaProduto;
    private javax.swing.JButton botaoSalvarNovoProd;
    private javax.swing.JDialog cadastrarNovoProduto;
    private javax.swing.JDialog entradaProduto;
    private javax.swing.JButton jButtonCancelarEntrada;
    private javax.swing.JButton jButtonCancelarSaida;
    private javax.swing.JButton jButtonSalvarEntrada;
    private javax.swing.JButton jButtonSalvarSaida;
    private javax.swing.JComboBox<String> jComboBoxProdutos;
    private javax.swing.JComboBox<String> jComboBoxProdutos1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabelIconeMais;
    private javax.swing.JLabel jLabelIconeMenos;
    private javax.swing.JLabel jLabelProduto;
    private javax.swing.JLabel jLabelProduto1;
    private javax.swing.JLabel jLabelProduto2;
    private javax.swing.JLabel jLabelQuantidade;
    private javax.swing.JLabel jLabelQuantidade1;
    private javax.swing.JLabel jLabelQuantidade2;
    private javax.swing.JLabel jLabelQuantidade3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableProdutos;
    private javax.swing.JTextField jTextFieldNome;
    private javax.swing.JFormattedTextField jTextFieldQuantidade;
    private javax.swing.JFormattedTextField jTextFieldQuantidade1;
    private javax.swing.JFormattedTextField jTextFieldQuantidadeProd;
    private javax.swing.JFormattedTextField jTextFieldUnid;
    private javax.swing.JMenuItem listarTodosPdf;
    private javax.swing.JLabel logoJanelaPrincipal;
    private javax.swing.JMenuBar menu;
    private javax.swing.JMenu opNovo;
    private javax.swing.JMenuItem opNovoProduto;
    private javax.swing.JMenu relatorio;
    private javax.swing.JDialog saidaProduto;
    private javax.swing.JMenu sobre;
    // End of variables declaration//GEN-END:variables
}
